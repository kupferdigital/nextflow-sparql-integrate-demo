process sparqlIntegrate {
    
    input:
    path input from Channel.fromPath('./*.ttl')

    output:
    stdout results

    """
    #!/usr/bin/env -S sparql-integrate --jq $input

    prefix dc11:  <http://purl.org/dc/elements/1.1/>
    prefix owl:   <http://www.w3.org/2002/07/owl#>
    prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    prefix xsd:   <http://www.w3.org/2001/XMLSchema#>

    SELECT ?version WHERE {

      ?ontology a owl:Ontology;
          owl:priorVersion ?version .
    }
    """

}

process jq {

    input:
    stdin results

    output:
    stdout output

    """
    jq -r '.[0].version'
    """

}

output.view {"$it"}
