# Demo

1. Install [nextflow](https://www.nextflow.io/), [sparql-integrate](https://github.com/SmartDataAnalytics/RdfProcessingToolkit), [jq](https://stedolan.github.io/jq/)

1. Run nextflow:

    ```bash
    nextflow run demo.nf
    ```
